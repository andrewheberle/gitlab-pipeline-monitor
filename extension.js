const { execFileSync } = require('child_process');
const {
  window,
  StatusBarAlignment,
  workspace,
  commands,
  Uri
} = require('vscode');
const request = require('request-promise-native');
const moment = require('moment');

const createGitLabClient = (server, project, token) => {
  const url = _ =>
    `${server}/api/v4/projects/${encodeURIComponent(project)}${_}`;

  return endpoint =>
    request({
      uri: url(endpoint),
      json: true,
      headers: {
        'PRIVATE-TOKEN': token
      },
      rejectUnauthorized: workspace.getConfiguration(`http`)['proxyStrictSSL'],
      resolveWithFullResponse: true,
      simple: false
    })
      .then(x => {
        if (x.statusCode <= 400) return x;
        else throw new Error(`${x.body.message}`);
      })
      .then(response => response.body);
};

const createStatusBarItem = () => {
  const item = window.createStatusBarItem(StatusBarAlignment.Left, Infinity);
  item.show();
  item.command = 'pipeline.openInBrowser';
  return Object.assign({}, item, {
    showText: txt => {
      item.text = `$(git-commit) pipeline: ${txt}`;
    }
  });
};

const gitClient = ws => (...args) =>
  execFileSync('git', [`--git-dir`, `${ws.uri.fsPath}/.git/`, ...args])
    .toString()
    .trim();

const repo = () =>
  new Promise((resolve, reject) => {
    try {
      const ws = workspace.workspaceFolders[0];
      const git = gitClient(ws);

      const hash = git('rev-parse', 'HEAD');
      const branch = git('name-rev', '--name-only', hash)
        .replace('tags/', '')
        .trim();
      const url = git('config', '--get', 'remote.origin.url');

      let domain = null;
      let project = null;

      if (url.includes('git@')) {
        const origin = url.split(':');
        domain = origin[0].replace('git@', '');
        project = origin[1].replace('.git', '').trim();
      } else {
        const { hostname, path } = require('url').parse(url);
        domain = hostname;
        project = path.replace('.git', '').trim();
      }

      const info = { domain, project, branch };
      resolve(info);
    } catch (err) {
      console.log(err);
      reject(err);
    }
  });

const getToken = domain => {
  const config = workspace.getConfiguration(`gitlabPipelineMonitor`);
  const token = config['tokens'][domain];
  return token;
};

const getInterval = () => {
  const config = workspace.getConfiguration(`gitlabPipelineMonitor`);
  const intervalFromSetting = parseInt(config['interval']);
  return intervalFromSetting;
};

const getLatestPipeline = async () => {
  const { domain, project, branch } = await repo();
  const token = getToken(domain);
  if (!token) throw new Error(`No token for '${domain}'`);
  const getData = createGitLabClient(`https://${domain}`, project, token);
  const [{ id }] = await getData(`/pipelines/?ref=${branch}`);
  const pipeline = await getData(`/pipelines/${id}`);
  return Object.assign({}, pipeline, {
    webUrl: `https://${domain}/${project}/pipelines/${id}/`
  });
};

const getVsCodeSymbol = status => {
  switch (status) {
    case 'success':
      return '$(check)';
    case 'failed':
      return '$(x)';
    case 'running':
      return '$(triangle-right)';
    case 'pending':
      return '$(clock)';
    default:
      return '';
  }
};

const updateMonitor = async showText => {
  try {
    const pipeline = await getLatestPipeline();
    const { status, finished_at, ref, webUrl } = pipeline;
    pipelineWebUrl = webUrl;
    const time = finished_at ? moment(finished_at).fromNow() : '';
    const symbol = getVsCodeSymbol(status);
    const text = `${symbol} ${status.toUpperCase()} on '${ref}' ${time}`;
    showText(text.trim());
  } catch (err) {
    showText(`$(alert) ${err.message}`);
    console.log(err);
  }
};

let stopMonitoring = null;
let pipelineWebUrl = null;
exports.activate = async context => {
  const cmd = commands.registerCommand('pipeline.openInBrowser', () => {
    if (pipelineWebUrl === null) return;
    commands.executeCommand('vscode.open', Uri.parse(pipelineWebUrl));
  });
  context.subscriptions.push(cmd);

  const item = createStatusBarItem();
  context.subscriptions.push(item);

  const { showText } = item;
  showText('...');
  await updateMonitor(showText);
  const loop = setInterval(async () => {
    await updateMonitor(showText);
  }, getInterval());
  stopMonitoring = () => clearInterval(loop);
};

exports.deactivate = () => {
  if (stopMonitoring !== null) stopMonitoring();
};
