[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# gitlab-pipeline-monitor

> This extension monitors your latest GitLab Pipeline on the current branch

## Features

* shows the latest state of the pipeline in the statusbar

  * ![pending](.assets/pending.png)
  * ![running](.assets/running.png)
  * ![success](.assets/success.png)
  * ![failed](.assets/failed.png)

* statusbar item is clickable, opens the pipeline view in the default browser

## Requirements

The extension gets activated only if the workspace contains a `.gitlab-ci.yml` file.

## Extension Settings

* `gitlabPipelineMonitor.interval`: set interval time in milliseconds for API polling (default: 10000)
* `gitlabPipelineMonitor.tokens`: Private tokens for the different gitlab deployments

  ```json
  "gitlabPipelineMonitor.tokens": {
     "gitlab.com": "myPrivateTokenToGitlab.Com",
     "myprivateGitlab.co": "myToken"
  }
  ```

> how to create [Gitlab Personal Access Token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html)

**Hint**: If you are using private gitlab deployment with self-signed cert, you should set the `http.proxyStrictSSL` to `false`. More details: <https://gitlab.com/balazs4/gitlab-pipeline-monitor/issues/3>

## Known Issues

Currently no `multi-workspace` support.

## Author

balazs4

* <https://twitter.com/balazs4>
* <https://gitlab.com/balazs4>
* <https://github.com/balazs4>

## Release Notes

<https://gitlab.com/balazs4/gitlab-pipeline-monitor/tags>